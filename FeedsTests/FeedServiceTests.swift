//
//  FeedServiceTests.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import XCTest
@testable import Feeds

class FeedServiceTests: XCTestCase {
    var feedService: FeedServiceImplementation!
    
    override func setUp() {
        super.setUp()

        let bundle = Bundle(for: type(of: self))
        let sessionManagerMock = SessionManagerMock(bundle: bundle)
        let networkClient = NetworkClient(sessionManager: sessionManagerMock, domain: "")
        feedService = FeedServiceImplementation(networkClient: networkClient)
    }

    func testOnSuccessfullyObtainFeeds() {
        let feedsExpectation = expectation(description: "Feeds successfully loaded")

        feedService.obtainFeed(filter: .community, pageNumber: 1) { (result) in
            switch result {
            case .success(_):
                feedsExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        wait(for: [feedsExpectation], timeout: .defaultTimeout)
    }
}

private struct SessionManagerMock:SessionManagerProtocol {
    let bundle: Bundle

    func performRequest(url: String, method: APIMethod, parameters: APIParameters?, headers: APIHeaders?, completion: APICompletion?) {
        let url = bundle.url(forResource: "feeds", withExtension: "json")
        let data = try! Data(contentsOf: url!)

        completion?(.success(data))
    }
}
