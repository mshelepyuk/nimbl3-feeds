//
//  AuthorizationServiceTests.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import XCTest
@testable import Feeds

class AuthorizationServiceTests: XCTestCase {

    private var authorizationService: AuthorizationService!
    private var tokenStorage = TokenStorageMock()
    
    override func setUp() {
        super.setUp()

        let bundle = Bundle(for: type(of: self))
        let sessionManagerMock = SessionManagerMock(bundle: bundle)
        let networkClient = NetworkClient(sessionManager: sessionManagerMock, domain: "")
        authorizationService = AuthorizationServiceImplementation(tokenStorage: tokenStorage, networkClient: networkClient)
    }

    func testOnTokenSuccessfullyLoadedAndSaved() {
        let tokenExpectation = expectation(description: "Token successfully loaded")

        authorizationService.login(credentials: CredentialsEntity(email: "Colin", password: "Mc Rally")) { (result) in
            switch result {
            case .success(_):
                XCTAssert(self.tokenStorage.tokenSaved, "Token not saved")
                tokenExpectation.fulfill()
            case .failure(let error):
                XCTFail("Token loading failed - \(error.localizedDescription)")
            }
        }

        wait(for: [tokenExpectation], timeout: .defaultTimeout)
    }

}

private class TokenStorageMock: TokenStorage {

    var tokenSaved = false

    func obtain() throws -> TokenEntity? {
        return TokenEntity(accessToken: "test", refreshToken: "test", tokenType: "test")
    }

    func save(token: TokenEntity) throws {
        tokenSaved = true
    }
}

private struct SessionManagerMock: SessionManagerProtocol {
    let bundle: Bundle

    func performRequest(url: String, method: APIMethod, parameters: APIParameters?, headers: APIHeaders?, completion: APICompletion?) {
        let url = bundle.url(forResource: "token", withExtension: "json")
        let data = try! Data(contentsOf: url!)

        completion?(.success(data))
    }
}
