//
//  TimeInterval.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

extension TimeInterval {
    static var defaultTimeout: TimeInterval = 10
}
