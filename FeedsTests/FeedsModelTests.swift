//
//  FeedsModelTests.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import XCTest
@testable import Feeds

class FeedsModelTests: XCTestCase {

    var feedsModel: FeedsModel!
    var feedsModelDelegate: FeedsModelMockDelegate!
    var feedsService: FeedService!
    
    override func setUp() {
        super.setUp()
        feedsModelDelegate = FeedsModelMockDelegate()
        let bundle = Bundle(for: type(of: self))
        feedsService = FeedsServiceMock(bundle: bundle)
        feedsModel = FeedsModel(feedService: feedsService, delegate: feedsModelDelegate)
    }
    
    override func tearDown() {
        feedsModel = nil
        feedsService = nil
        feedsModelDelegate = nil
        super.tearDown()
    }

    func testOnLoadingCalled() {
        feedsModel.obtainFeeds()
        XCTAssertTrue(feedsModelDelegate.isLoadingCalled, "Loading method wasn't called")
    }

    func testOnSuccessCalled() {
        feedsModel.obtainFeeds()
        XCTAssertTrue(feedsModelDelegate.isSuccessFuncCalled, "Success method wasn't called")
    }

    func testOnFailedCalled() {
        feedsService = FeedsServiceFailedMock()
        feedsModel = FeedsModel(feedService: feedsService, delegate: feedsModelDelegate)
        XCTAssertTrue(feedsModelDelegate.isFailedFuncCalled, "Failed method wasn't called")
    }
}

class FeedsModelMockDelegate: FeedsModelDelegate {
    var isLoadingCalled = false
    var isSuccessFuncCalled = false
    var isFailedFuncCalled = false

    func feedsSuccessfullyLoaded(_ model: FeedsModel) {
        isSuccessFuncCalled = true
    }

    func feedsIsLoading(_ model: FeedsModel) {
        isLoadingCalled = true
    }

    func feedsLoadingFail(_ model: FeedsModel, error: Error) {
        isFailedFuncCalled = true
    }
}

struct FeedsServiceMock: FeedService {
    let bundle: Bundle

    func obtainFeed(filter: FeedFilter, pageNumber: Int, completion: FeedServiceCompletion?) {
        let url = bundle.url(forResource: "feeds", withExtension: "json")
        let data = try! Data(contentsOf: url!)

        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .formatted(.iso8601Full)
        let feedResponseEntity = try! jsonDecoder.decode(FeedResponseEntity.self, from: data)
        completion?(.success(feedResponseEntity))
    }
}

struct FeedsServiceFailedMock: FeedService {
    func obtainFeed(filter: FeedFilter, pageNumber: Int, completion: FeedServiceCompletion?) {
        completion?(.failure(MockError.someError))
    }
}

enum MockError: Error {
    case someError
}


