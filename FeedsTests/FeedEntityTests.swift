//
//  FeedEntityTests.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import XCTest
@testable import Feeds

class FeedEntityTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }

    func testOnDecodingSuccess() {
        let json = """
        {
            "id": "TestId",
            "type": "TestType",
            "attributes": {
                "cover_image_url": "some_image_url",
                "created_at": "2017-11-16T02:05:48.583Z"
            },
            "relationships": {
                "user": {
                    "data": {
                        "id": "10147",
                        "type": "users"
                    }
                }
            }
        }
        """

        let data = json.data(using: .utf8)!

        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .formatted(.iso8601Full)

        do {
            let feed = try jsonDecoder.decode(FeedEntity.self, from: data)
            XCTAssertTrue(feed.id == "TestId")
            let correctDate = DateFormatter.iso8601Full.date(from: "2017-11-16T02:05:48.583Z")
            XCTAssertTrue(feed.id == "TestId", "Id mapping failed")
            XCTAssertTrue(feed.type == "TestType", "Type mapping failed")
            XCTAssertTrue(feed.attributes.createdAt == correctDate, "Date mapping failed")
            XCTAssertTrue(feed.attributes.coverImageURL == "some_image_url", "image url mapping failed")
            XCTAssertTrue(feed.relationships.user.data.id == "10147", "userId mapping failed")
        } catch {
            XCTFail("Decoding failed - \(error.localizedDescription)")
        }
    }
    
}
