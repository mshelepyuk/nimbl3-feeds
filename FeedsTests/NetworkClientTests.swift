//
//  NetworkClientTests.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import XCTest
@testable import Feeds

class NetworkClientTests: XCTestCase {
    private var request = APIRequest(path: "", method: .post, parameters: [:], headers: [:])
    private var sessionManager = SessionManagerMock()
    private var networkClient: NetworkClient!
    
    override func setUp() {
        super.setUp()

        networkClient = NetworkClient(sessionManager: sessionManager, domain: "")
    }

    func testOnDataWasLoaded() {
        let dataExpectation = expectation(description: "Data was loaded successfully")

        networkClient.perform(request: request) { (result) in
            switch result {
            case .success(_):
                dataExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        wait(for: [dataExpectation], timeout: .defaultTimeout)
    }
}


private class SessionManagerMock: SessionManagerProtocol {
    var requestPerformed = false

    func performRequest(url: String, method: APIMethod, parameters: APIParameters?, headers: APIHeaders?, completion: APICompletion?) {
        requestPerformed = true

        let data = "{}".data(using: .utf8)
        completion?(.success(data!))
    }
}

