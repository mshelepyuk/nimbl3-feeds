//
//  IncludedTests.swift
//  FeedsTests
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import XCTest
@testable import Feeds

class IncludedTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }

    func testOnDecodingSuccess() {
        let json = """
                        {
                            "id": "10147",
                            "type": "users",
                            "attributes": {
                                "first_name": "Julien",
                                "last_name": "Empty",
                                "avatar": "https://cdn.filestackcontent.com/mECEuCDxQya743D4pAVw",
                                "location": "Bangkok, Thailand",
                                "points_count": "130",
                                "friendships_count": 2,
                                "countries_count": 1,
                                "friend_request": {},
                                "is_friend": false,
                                "name": "Julien Empty"
                            }
                        }
        """

        let data = json.data(using: .utf8)!

        let jsonDecoder = JSONDecoder()
        let includedEntity = try! jsonDecoder.decode(IncludedEntity.self, from: data)
        XCTAssertTrue(includedEntity.id == "10147", "Id mapping failed")
        XCTAssertTrue(includedEntity.type == "users", "Type mapping failed")
        XCTAssertTrue(includedEntity.attributes?.avatar == "https://cdn.filestackcontent.com/mECEuCDxQya743D4pAVw", "Avatar mapping failed")
        XCTAssertTrue(includedEntity.attributes?.name == "Julien Empty", "Name mapping failed")
    }
}
