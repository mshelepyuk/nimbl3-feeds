//
//  TokenStorageImplementation.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation
import KeychainAccess

enum TokenStorageError: Error {
    case invalidToken

    var localizedDescription: String {
        return "Invalid Token"
    }
}

struct TokenStorageImplementation {
    let keychain: Keychain
}

extension TokenStorageImplementation: TokenStorage {

    func obtain() throws -> TokenEntity? {
        guard let tokenData = keychain[data: .tokenKey] else {
            return nil
        }
        let jsonDecoder = JSONDecoder()
        let tokenEntity = try jsonDecoder.decode(TokenEntity.self, from: tokenData)
        return tokenEntity
    }

    func save(token: TokenEntity) throws {
        let jsonEncoder = JSONEncoder()
        let data = try jsonEncoder.encode(token)
        keychain[data: .tokenKey] = data
    }
}

private extension String {
    static var tokenKey = "tokenKey"
}
