//
//  ServiceAPI.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Alamofire

typealias APICompletion = (_ result: APIResult<Data>) -> Void
typealias APIParameters = [String: Any]
typealias APIHeaders = [String: String]
typealias APIMethod = HTTPMethod

enum APIResult<T> {
    case success(T)
    case failure(Error)
}

struct APIRequest {
    let path: String
    let method: APIMethod
    let parameters: APIParameters?
    let headers: APIHeaders?
}
