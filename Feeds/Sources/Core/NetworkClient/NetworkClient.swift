//
//  NetworkClient.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Alamofire

struct NetworkClient {
    let sessionManager: SessionManagerProtocol
    let domain: String

    init(sessionManager: SessionManagerProtocol = SessionManager.default, domain: String = "https://staging.travelbook.com/") {
        self.sessionManager = sessionManager
        self.domain = domain
    }

    func perform(request: APIRequest, completion: APICompletion?) {
        sessionManager.performRequest(
            url: .makeURL(domain: domain, path: request.path),
            method: request.method,
            parameters: request.parameters,
            headers: request.headers,
            completion: completion
        )
    }
}

private extension String {

    static func makeURL(domain: String, path: String) -> String {
        return "\(domain)\(path)"
    }
}


