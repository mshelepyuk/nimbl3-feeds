//
//  AuthorizationService.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

typealias AuthorizationServiceCompletion = (APIResult<Bool>) -> Void

protocol AuthorizationService {
    func login(credentials: CredentialsEntity, completion: AuthorizationServiceCompletion?)
}
