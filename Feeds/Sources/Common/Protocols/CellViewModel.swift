//
//  CellViewModel.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit

protocol CellViewAnyModelType {
    static func cellClass() -> UIView.Type
    func setupDefault(on cell: UIView)
}

protocol CellViewModelType: CellViewAnyModelType {
    associatedtype CellClass: UIView
    func setup(on cell: CellClass)
}

extension CellViewModelType {
    static func cellClass() -> UIView.Type {
        return Self.CellClass.self
    }

    func setupDefault(on cell: UIView) {
        setup(on: cell as! Self.CellClass)
    }
}
