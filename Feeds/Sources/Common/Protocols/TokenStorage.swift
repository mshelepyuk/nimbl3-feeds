//
//  TokenStorage.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

protocol TokenStorage {
    func obtain() throws -> TokenEntity?
    func save(token: TokenEntity) throws
}
