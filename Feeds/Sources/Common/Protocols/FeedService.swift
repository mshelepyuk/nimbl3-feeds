//
//  FeedService.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

typealias FeedServiceCompletion = (_ result: APIResult<FeedResponseEntity>) -> Void

protocol FeedService {
    func obtainFeed(filter: FeedFilter, pageNumber: Int, completion: FeedServiceCompletion?)
}
