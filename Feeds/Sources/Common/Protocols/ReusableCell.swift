//
//  ReusableCell.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit

protocol ReusableCell {
    static var identifier: String { get }
    static var nib: UINib { get }
}
