//
//  SessionManagerProtocol.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Alamofire

protocol SessionManagerProtocol {
    func performRequest(url: String, method: APIMethod, parameters: APIParameters?, headers: APIHeaders?, completion: APICompletion?)
}
