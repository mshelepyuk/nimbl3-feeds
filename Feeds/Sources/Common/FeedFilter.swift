//
//  FeedFilter.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

enum FeedFilter: String {
    case friends
    case community
}
