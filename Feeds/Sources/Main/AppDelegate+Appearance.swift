//
//  AppDelegate+Appearance.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit

extension AppDelegate {

    func configureAppearance() {
        UINavigationBar.appearance().barTintColor = .white
    }
}
