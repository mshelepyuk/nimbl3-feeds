//
//  AuthorizationServiceImplementation.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation
import Alamofire

struct AuthorizationServiceImplementation {
    let tokenStorage: TokenStorage
    let networkClient: NetworkClient
}

extension AuthorizationServiceImplementation: AuthorizationService {

    func login(credentials: CredentialsEntity, completion: AuthorizationServiceCompletion?) {
        let parameters = [
            "client_id": "3bb0640f3232379a9e07c0c44f9ef5e764eefb9ba0e1d31168a90ecebe2bc67d",
            "client_secret": "073177b5f4f3489d46921c62629a42aa7b2bbdf57fc578bf2c61917957d037cc",
            "grant_type": "password",
            "email": credentials.email,
            "password": credentials.password
        ]
        let request = APIRequest(path: "api/v1/token", method: .post, parameters: parameters, headers: nil)

        networkClient.perform(request: request) { (result) in
            switch result {
            case .success(let data):
                do {
                    let jsonDecoder = JSONDecoder()
                    let tokenEntity = try jsonDecoder.decode(TokenEntity.self, from: data)
                    try self.tokenStorage.save(token: tokenEntity)
                    completion?(.success(true))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}

extension AuthorizationServiceImplementation: RequestAdapter {
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        guard let tokenEntity = try tokenStorage.obtain() else {
            return urlRequest
        }
        var urlRequest = urlRequest
        urlRequest.setValue("\(tokenEntity.tokenType) \(tokenEntity.accessToken)", forHTTPHeaderField: "Authorization")

        return urlRequest
    }
}

extension AuthorizationServiceImplementation: RequestRetrier {
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == .authorizationErrorStatusCode else {
            completion(false, 0.0)
            return
        }
        let credentialsEntity = CredentialsEntity(email: "olivier@nimbl3.com", password: "12345678")
        login(credentials: credentialsEntity) { (result) in
            switch result {
            case .success(_):
                completion(true, 0.0)
            case .failure(_):
                completion(false, 0.0)
            }
        }
    }
}

private extension Int {
    static var authorizationErrorStatusCode = 401
}
