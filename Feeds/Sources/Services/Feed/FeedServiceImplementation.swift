//
//  FeedServiceImplementation.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

struct FeedServiceImplementation {
    let networkClient: NetworkClient
}

extension FeedServiceImplementation: FeedService {

    func obtainFeed(filter: FeedFilter, pageNumber: Int, completion: FeedServiceCompletion?) {
        let path = "api/v1/feeds?page=\(pageNumber)&filter[scope]=\(filter.rawValue)"
        let request = APIRequest(
            path: path,
            method: .get,
            parameters: nil,
            headers: nil
        )

        networkClient.perform(request: request) { (result) in
            switch result {
            case .success(let data):
                do {
                    let jsonDecoder = JSONDecoder()
                    jsonDecoder.dateDecodingStrategy = .formatted(.iso8601Full)
                    let feedResponse = try jsonDecoder.decode(FeedResponseEntity.self, from: data)
                    completion?(.success(feedResponse))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}
