//
//  FilterSelectionView.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit
import HMSegmentedControl

protocol FilterSelectionViewDelegate: class {
    func filterSelection(_ filterSelectionView: FilterView, didSelectFilterAt index: Int)
}

protocol FilterSelectionViewDataSource: class {
    func numberOfFilters(in filterSelectionView: FilterView) -> Int
    func titleForFilter(in filterSelectionView: FilterView, at index: Int) -> String
}

class FilterView: UIView {
    @IBOutlet weak var titleButton: UIButton! {
        didSet {
            titleButton.setTitle("Travelbooks", for: .normal)
            titleButton.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
            titleButton.titleLabel?.font = .titleButton
            titleButton.setTitleColor(.title, for: .normal)
        }
    }

    @IBOutlet weak var segmentedControl: HMSegmentedControl! {
        didSet {
            segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged(_:)), for: .valueChanged)
            segmentedControl.selectionStyle = .box
            segmentedControl.selectionIndicatorBoxColor = .clear
            segmentedControl.selectionIndicatorHeight = 0
            segmentedControl.titleTextAttributes = [
                kCTForegroundColorAttributeName: UIColor.title,
                kCTFontAttributeName: UIFont.segmentedControlBar
            ]
            segmentedControl.selectedTitleTextAttributes = [
                kCTForegroundColorAttributeName: UIColor.selectedFilter,
                kCTFontAttributeName: UIFont.segmentedControlBar
            ]
        }
    }

    @IBOutlet weak var sortByLabel: UILabel! {
        didSet {
            sortByLabel.font = .sortBy
            sortByLabel.textColor = .title
        }
    }

    @IBOutlet weak var indicatorImageView: UIImageView!

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    weak var delegate: FilterSelectionViewDelegate?
    weak var dataSource: FilterSelectionViewDataSource? {
        didSet {
            configureSegmentedControl()
        }
    }

    private var needsShowFilters = false {
        didSet {
            heightConstraint.constant = needsShowFilters ? .expandedHeight : .hiddenHeight
            indicatorImageView.image = needsShowFilters ? #imageLiteral(resourceName: "icon-dropup") : #imageLiteral(resourceName: "icon-dropdown")
            layoutIfNeeded()
        }
    }
}

extension FilterView {

    override func awakeFromNib() {
        super.awakeFromNib()

        needsShowFilters = false
        clipsToBounds = true
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowOpacity = 1
        layer.shouldRasterize = true
        layer.shadowColor = UIColor.shadow.cgColor
    }
}

private extension FilterView {

    func configureSegmentedControl() {
        guard let numberOfBars = dataSource?.numberOfFilters(in: self) else { return }
        var barTitles: [String] = []

        for barIndex in 0..<numberOfBars {
            let barTitle = dataSource?.titleForFilter(in: self, at: barIndex) ?? ""
            barTitles.append(barTitle)
        }
        segmentedControl.sectionTitles = barTitles
    }
}

private extension FilterView {

    @objc
    func buttonDidTapped() {
        needsShowFilters = !needsShowFilters
    }

    @objc
    func segmentedControlValueChanged(_ segmentedControl: UISegmentedControl) {
        needsShowFilters = false
        delegate?.filterSelection(self, didSelectFilterAt: segmentedControl.selectedSegmentIndex)
    }
}

// MARK: - Constants
private extension CGFloat {

    static var hiddenHeight: CGFloat = 0.0
    static var expandedHeight: CGFloat = 128.0
}

private extension UIFont {

    static var titleButton = UIFont.Pluto.bold(ofSize: 22.0)
    static var sortBy = UIFont.Pluto.medium(ofSize: 14.0)
    static var segmentedControlBar = UIFont.Pluto.medium(ofSize: 14.0)
}
