//
//  FeedCollectionViewCell.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit
import SDWebImage

class FeedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView! {
        didSet {
            backView.clipsToBounds = true
            backView.layer.shadowOffset = .zero
            backView.layer.cornerRadius = .backViewCornerRadius
            backView.layer.shadowOpacity = 1
            backView.layer.shouldRasterize = true
            backView.layer.shadowColor = UIColor.shadow.cgColor
        }
    }

    @IBOutlet weak var avatarImageView: UIImageView! {
        didSet {
            avatarImageView.layer.cornerRadius = avatarImageView.bounds.size.height / 2
            avatarImageView.clipsToBounds = true
            avatarImageView.sd_setIndicatorStyle(.whiteLarge)
            avatarImageView.sd_setShowActivityIndicatorView(true)
        }
    }

    @IBOutlet weak var userNameLabel: UILabel! {
        didSet {
            userNameLabel.font = .userName
        }
    }

    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = .date
            dateLabel.textAlignment = .center
        }
    }

    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = .white
            descriptionLabel.font = .description
        }
    }

    @IBOutlet weak var postImageView: UIImageView! {
        didSet {
            postImageView.backgroundColor = .lightGray
            postImageView.sd_setIndicatorStyle(.whiteLarge)
            postImageView.sd_setShowActivityIndicatorView(true)
        }
    }

    @IBOutlet weak var descriptionView: UIView! {
        didSet {
            descriptionView.backgroundColor = .darkOpaque
            descriptionView.layer.cornerRadius = .descriptionViewCornerRadius
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - Constants
private extension CGFloat {
    static var backViewCornerRadius: CGFloat = 12.0
    static var descriptionViewCornerRadius: CGFloat = 6.0
}

// TODO: Make styles
private extension UIFont {
    static var userName = Pluto.medium(ofSize: 14.0)
    static var date = Pluto.medium(ofSize: 10.0)
    static var description = Pluto.medium(ofSize: 12.0)
}
