//
//  FeedsViewController.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit
import Alamofire

class FeedsViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.backgroundColor = .background
            collectionView.registerNib(for: FeedCollectionViewCell.self)
            collectionView.addSubview(refreshControl)
        }
    }

    @IBOutlet weak var filterView: FilterView!

    private var model: FeedsModel!

    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(obtainFeeds), for: .valueChanged)
        return refreshControl
    }()
}

// MARK: - UIViewController
extension FeedsViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        model = .makeFeedsModel(delegate: self)
        filterView.dataSource = self
        filterView.delegate = self
        configureInterface()
    }
}

// MARK: - Configuration
private extension FeedsViewController {

    func configureInterface() {
        configureNavigationBar()
    }

    func configureNavigationBar() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.leftBarButtonItem = .makeBarButtonItem(with: #imageLiteral(resourceName: "icon-search-black"))
        navigationItem.rightBarButtonItem = .makeBarButtonItem(with: #imageLiteral(resourceName: "icon-settings"))
    }
}

// MARK: - Actions
private extension FeedsViewController {

    @objc
    func obtainFeeds() {
        model.obtainFeeds()
    }
}

// MARK: - UICollectionView Data source
extension FeedsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.numberOfFeeds()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = model.feedViewModel(at: indexPath.row)
        return collectionView.dequeueReusableCell(for: indexPath, with: cellModel)
    }
}

// MARK: - UICollectionViewDelegate & UICollectionViewDelegateFlowLayout
extension FeedsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width - 10 * 2
        var height: CGFloat = 270

        if let cell = collectionView.cellForItem(at: indexPath) as? FeedCollectionViewCell {
            height = cell.systemLayoutSizeFitting(.zero).height
        }
        return CGSize(
            width: width,
            height: height
        )
    }
}

// MARK: - FeedsModel Delegate
extension FeedsViewController: FeedsModelDelegate {
    func feedsSuccessfullyLoaded(_ model: FeedsModel) {
        refreshControl.endRefreshing()
        hideHUD()
        collectionView.reloadData()
    }

    func feedsIsLoading(_ model: FeedsModel) {
        if refreshControl.isRefreshing {
            return
        }
        showHUD()
    }

    func feedsLoadingFail(_ model: FeedsModel, error: Error) {
        refreshControl.endRefreshing()
        hideHUD()
        showAlert(error: error)
    }
}

// MARK: - FilterSelectionViewDataSource
extension FeedsViewController: FilterSelectionViewDataSource {

    func numberOfFilters(in filterSelectionView: FilterView) -> Int {
        return model.numberOfFilters()
    }

    func titleForFilter(in filterSelectionView: FilterView, at index: Int) -> String {
        return model.titleForFilter(at: index)
    }
}

// MARK: - FilterSelectionViewDelegate
extension FeedsViewController: FilterSelectionViewDelegate {
    func filterSelection(_ filterSelectionView: FilterView, didSelectFilterAt index: Int) {
        let filter = model.filter(at: index)
        model.set(filter: filter)
    }
}
