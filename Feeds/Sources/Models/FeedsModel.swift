//
//  FeedsModel.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

protocol FeedsModelDelegate: class {
    func feedsSuccessfullyLoaded(_ model: FeedsModel)
    func feedsIsLoading(_ model: FeedsModel)
    func feedsLoadingFail(_ model: FeedsModel, error: Error)
}

class FeedsModel {
    let feedService: FeedService

    weak var delegate: FeedsModelDelegate?

    private lazy var feedFilters: [FeedFilter] = [.friends, .community]

    private(set) var currentFilter: FeedFilter = .friends {
        didSet {
            obtainFeeds()
        }
    }

    private var feeds: [FeedEntity] = []
    private var users: Set<UserEntity> = []

    private(set) var state: State = .loading {
        didSet {
            stateDidChanged()
        }
    }

    init(feedService: FeedService, delegate: FeedsModelDelegate) {
        self.feedService = feedService
        self.delegate = delegate

        obtainFeeds()
    }
}

extension FeedsModel {

    func obtainFeeds() {
        state = .loading
        feedService.obtainFeed(filter: currentFilter, pageNumber: 1) { [weak self] (result) in
            switch result {
            case .success(let feeds):
                self?.feeds = feeds.feeds
                self?.state = .success
                feeds.users.forEach({ (user) in
                    self?.users.insert(user)
                })
            case .failure(let error):
                self?.state = .failure(error)
            }
        }
    }

    func numberOfFeeds() -> Int {
        return feeds.count
    }

    func feedViewModel(at index: Int) -> FeedCollectionViewCellModel {
        let feedEntity = feeds[index]

        if let userIndex = users.index(where: { $0.id == feedEntity.relationships.user.data.id }) {
            let user = users[userIndex]
            return FeedCollectionViewCellModel(feedEntity: feedEntity, user: user)
        }
        return FeedCollectionViewCellModel(feedEntity: feedEntity, user: nil)
    }
}

extension FeedsModel {

    func set(filter: FeedFilter) {
        currentFilter = filter
    }

    func numberOfFilters() -> Int {
        return feedFilters.count
    }

    func titleForFilter(at index: Int) -> String {
        return feedFilters[index].rawValue.capitalized
    }

    func filter(at index: Int) -> FeedFilter {
        return feedFilters[index]
    }
}

private extension FeedsModel {

    func stateDidChanged() {
        switch state {
        case .loading:
            delegate?.feedsIsLoading(self)
        case .success:
            delegate?.feedsSuccessfullyLoaded(self)
        case .failure(let error):
            delegate?.feedsLoadingFail(self, error: error)
        }
    }
}

extension FeedsModel {

    enum State {
        case loading
        case success
        case failure(Error)
    }
}
