//
//  FeedEntity.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

struct FeedEntity: Codable {
    let id: String
    let type: String
    let attributes: Attributes
    let relationships: Relationships
}

extension FeedEntity {

    struct Attributes: Codable {
        let createdAt: Date
        let coverImageURL: String

        enum CodingKeys: String, CodingKey {
            case createdAt = "created_at"
            case coverImageURL = "cover_image_url"

        }
    }
}

extension FeedEntity {

    struct Relationships: Codable {
        let user: User
    }
}

extension FeedEntity.Relationships {

    struct User: Codable {
        let data: UserData
    }
}

extension FeedEntity.Relationships.User {

    struct UserData: Codable {
        let id: String
    }
}
