//
//  FeedResponseEntity.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

struct FeedResponseEntity: Codable {
    let feeds: [FeedEntity]
    let included: [IncludedEntity]

    enum CodingKeys: String, CodingKey {
        case feeds = "data"
        case included
    }
}

extension FeedResponseEntity {

    var users: Set<UserEntity> {
        let users = included.filter { $0.type == "users" }
        let usersEntities = users.compactMap { return UserEntity(id: $0.id, name: $0.attributes?.name ?? "", avatar: $0.attributes?.avatar ?? "") }

        return Set(usersEntities)
    }
}
