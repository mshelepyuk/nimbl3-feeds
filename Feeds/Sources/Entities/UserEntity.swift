//
//  UserEntity.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

struct UserEntity: Hashable {
    let id: String
    let name: String
    let avatar: String
}
