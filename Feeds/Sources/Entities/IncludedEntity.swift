//
//  IncludedEntity.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

struct IncludedEntity: Codable {
    let id: String
    let type: String
    let attributes: Attributes?
}

extension IncludedEntity {

    struct Attributes: Codable {
        let avatar: String?
        let name: String?
    }
}
