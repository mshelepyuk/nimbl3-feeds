//
//  CredentialsEntity.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation

struct CredentialsEntity {
    let email: String
    let password: String
}
