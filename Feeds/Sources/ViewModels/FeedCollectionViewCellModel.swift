//
//  FeedCollectionViewCellModel.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation
import SDWebImage

struct FeedCollectionViewCellModel {
    let feedEntity: FeedEntity
    let user: UserEntity?
}

extension FeedCollectionViewCellModel: CellViewModelType {

    func setup(on cell: FeedCollectionViewCell) {
        if let url = URL(string: feedEntity.attributes.coverImageURL) {
            cell.postImageView.sd_setImage(with: url)
        }
        cell.dateLabel.text = .makeStringDate(from: feedEntity.attributes.createdAt)
        cell.descriptionLabel.text = "Chillin at Bali"

        if let user = user {
            if let avatarURL = URL(string: user.avatar) {
                cell.avatarImageView.sd_setImage(with: avatarURL, placeholderImage: #imageLiteral(resourceName: "icon-empty-profile"))
            }
            cell.userNameLabel.text = user.name
        }
    }
}

private extension String {

    static func makeStringDate(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM\nyyyy"
        return dateFormatter.string(from: date).uppercased()
    }
}
