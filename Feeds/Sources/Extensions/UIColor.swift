//
//  UIColor.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit.UIColor

extension UIColor {

    static var title = #colorLiteral(red: 0.2196078431, green: 0.2431372549, blue: 0.2235294118, alpha: 1)
    static var selectedFilter = #colorLiteral(red: 0.9254901961, green: 0.3176470588, blue: 0.4117647059, alpha: 1)
    static var filter = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
    static var darkOpaque = #colorLiteral(red: 0.07058823529, green: 0.07450980392, blue: 0.07058823529, alpha: 0.4)
    static var background = #colorLiteral(red: 0.9882352941, green: 0.9882352941, blue: 0.9882352941, alpha: 1)
    static var shadow = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
}
