//
//  UIVIewController.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {

    func showAlert(error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }

    func showHUD() {
        MBProgressHUD.showAdded(to: view, animated: true)
    }

    func hideHUD() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
