//
//  UIFont+Pluto.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit.UIFont

extension UIFont {

    enum Pluto {

        static func regular(ofSize size: CGFloat) -> UIFont {
            return .font(name: "PlutoSansCondRegular", size: size)
        }

        static func medium(ofSize size: CGFloat) -> UIFont {
            return .font(name: "PlutoSansCondMedium", size: size)
        }

        static func bold(ofSize size: CGFloat) -> UIFont {
            return .font(name: "PlutoSansCondBold", size: size)
        }
    }
}

private extension UIFont {

    static func font(name: String, size: CGFloat) -> UIFont {
        guard let font = UIFont(name: name, size: size) else {
            fatalError(.fontError(name))
        }
        return font
    }
}

private extension String {

    static func fontError(_ fontName: String) -> String {
        return "Font \(fontName) added incorrectly"
    }
}
