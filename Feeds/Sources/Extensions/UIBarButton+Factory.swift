//
//  UIBarButton+Factory.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit.UIBarButtonItem

extension UIBarButtonItem {

    static func makeBarButtonItem(with image: UIImage) -> UIBarButtonItem {
        let barButton = UIBarButtonItem(
            image: image.withRenderingMode(.alwaysOriginal),
            style: .plain,
            target: nil,
            action: nil
        )
        return barButton
    }
}
