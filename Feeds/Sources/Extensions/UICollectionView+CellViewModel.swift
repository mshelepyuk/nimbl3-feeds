//
//  UICollectionView+CellViewModel.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import UIKit.UICollectionView

extension UICollectionView {

    func dequeueReusableCell(for indexPath: IndexPath, with model: CellViewAnyModelType) -> UICollectionViewCell {
        let cellIdentifier = String(describing: type(of: model).cellClass())
        let cell = dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)

        model.setupDefault(on: cell)

        return cell
    }
}

