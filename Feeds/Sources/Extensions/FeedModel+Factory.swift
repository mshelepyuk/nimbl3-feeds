//
//  FeedModel+Factory.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Foundation
import Alamofire
import KeychainAccess

extension FeedsModel {

    static func makeFeedsModel(delegate: FeedsModelDelegate) -> FeedsModel {
        let feedsService = FeedServiceImplementation(networkClient: .make())
        let feedsModel = FeedsModel(feedService: feedsService, delegate: delegate)

        return feedsModel
    }
}


