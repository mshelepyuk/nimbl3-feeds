//
//  NetworkClient+Factory.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 27/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Alamofire
import KeychainAccess

extension NetworkClient {

    static func make() -> NetworkClient {
        let sessionManager = SessionManager.default
        let networkClient = NetworkClient()

        let keychain = Keychain(service: .keychainService)
        let tokenStorage = TokenStorageImplementation(keychain: keychain)
        let authorizationService = AuthorizationServiceImplementation(tokenStorage: tokenStorage, networkClient: networkClient)

        sessionManager.adapter = authorizationService
        sessionManager.retrier = authorizationService

        return networkClient
    }
}

private extension String {
    static var keychainService = "com.nimbl3.madmax.feeds.service"
}
