//
//  SessionManager+SessionProtocol.swift
//  Feeds
//
//  Created by Maxim Shelepyuk on 26/05/2018.
//  Copyright © 2018 Maxim Shelepyuk. All rights reserved.
//

import Alamofire

extension SessionManager: SessionManagerProtocol {
    func performRequest(url: String, method: APIMethod, parameters: APIParameters?, headers: APIHeaders?, completion: APICompletion?) {
        request(url, method: method, parameters: parameters, headers: headers)
            .validate()
            .responseData(completionHandler: { (response) in
                switch response.result {
                case .success(let data):
                    completion?(.success(data))
                case .failure(let error):
                    completion?(.failure(error))
                }
            })
    }
}
